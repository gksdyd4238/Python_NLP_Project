from __future__ import print_function
from gensim.models import KeyedVectors

# 모델 불러오기
model1 = KeyedVectors.load_word2vec_format('/Users/jngsoo/Desktop/fasttext/kakao_onlyNoun_model.vec')
model2 = KeyedVectors.load_word2vec_format('/Users/jngsoo/Desktop/fasttext/kakao_onlyNoun_model.vec')
 
# Getting the tokens 
# words = [
# for word in model1.vocab:
#     words.append(word)

# # Printing out number of tokens available
# print("Number of Tokens: {}".format(len(words)))

# # Printing out the dimension of a word vector 
# print("Dimension of a word vector: {}".format(
#     len(model1[words[0]])
# ))

# # Print out the vector of a word 
# print("Vector components of a word: {}".format(
#     model1[words[0]]
# ))

# print(words)
# print(words)

# if '블록체인' in words:
#     print("==============++++++++++==============++++++++++==============++++++++++==============++++++++++==============++++++++++")

# Test words 
word_add = ['채용']
word_sub = ['인력']

# Word vector addition and subtraction 
print("AFTER TFIDF MODEL")
for resultant_word in model2.most_similar(
    positive=word_add, negative=word_sub
):
    print("Word : {0} , Similarity: {1:.2f}".format(
        resultant_word[0], resultant_word[1]
    ))

# print("AFTER TFIDF MODEL WITHOUT SUB")
# for resultant_word in model2.most_similar(
#     positive=word_add
# ):
#     print("Word : {0} , Similarity: {1:.2f}".format(
#         resultant_word[0], resultant_word[1]
#     ))